package main

import (
	"log"
	"net/http"
)

func main() {
	// cards := newDeck()
	// cards.shuffle()
	// cards.print()

	http.HandleFunc("/", handler)

	log.Fatal(http.ListenAndServe(":8080", nil))

}

func handler(w http.ResponseWriter, r *http.Request) {

	w.Write([]byte("Hello World"))
}
